package tronscanapi

import (
	"fmt"
	"gitlab.com/golib4/http-client/http"
)

type Client struct {
	httpClient *http.Client
	apiKey     string
}

func NewClient(httpClient *http.Client, apiKey string) Client {
	return Client{
		httpClient: httpClient,
		apiKey:     apiKey,
	}
}

type GetAddressBalanceResponse struct {
	Total int         `json:"total"`
	Data  []TokenData `json:"data"`
}

type TokenData struct {
	Amount    string `json:"balance"`
	TokenName string `json:"tokenName"`
}

type GetAddressBalanceRequest struct {
	Address string
}

func (c Client) GetAddressBalance(request GetAddressBalanceRequest) (*GetAddressBalanceResponse, error) {
	var response GetAddressBalanceResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{
			Path:    fmt.Sprintf("/api/account/tokens?address=%s", request.Address),
			Headers: []http.HeaderData{{Name: "TRON-PRO-API-KEY", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing tronscanapi request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing tronscanapi request: %s", httpError)
	}

	return &response, nil
}

type TokenInfo struct {
	TokenId   string `json:"tokenId"`
	TokenName string `json:"tokenName"`
}

type TokenTransfer struct {
	TransactionID  string    `json:"transaction_id"`
	Status         int       `json:"status"`
	BlockTs        int64     `json:"block_ts"`
	FromAddress    string    `json:"from_address"`
	ToAddress      string    `json:"to_address"`
	Quant          string    `json:"quant"`
	ApprovalAmount string    `json:"approval_amount"`
	Confirmed      bool      `json:"confirmed"`
	FinalResult    string    `json:"finalResult"`
	TokenInfo      TokenInfo `json:"tokenInfo"`
}

type GetTokenTransfersResponse struct {
	TokenTransfers []TokenTransfer `json:"token_transfers"`
}

type GetTokenTransfersRequests struct {
	Limit          int32  `schema:"limit"`
	StartTimestamp int32  `schema:"start_timestamp"`
	Address        string `schema:"relatedAddress"`
}

func (c Client) GetTokenTransfers(request GetTokenTransfersRequests) (*GetTokenTransfersResponse, error) {
	var response GetTokenTransfersResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/filter/trc20/transfers",
			Headers: []http.HeaderData{{Name: "TRON-PRO-API-KEY", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing tronscanapi request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing tronscanapi request: %s", httpError)
	}

	return &response, nil
}

type GetBlocksRequest struct {
	Limit int32  `schema:"limit"`
	Sort  string `schema:"sort"`
}

type GetBlocksResponse struct {
	Data []struct {
		Number int64 `json:"number"`
	} `json:"data"`
}

func (c Client) GetBlocks(request GetBlocksRequest) (*GetBlocksResponse, error) {
	var response GetBlocksResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/block",
			Headers: []http.HeaderData{{Name: "TRON-PRO-API-KEY", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing tronscanapi request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing tronscanapi request: %s", httpError)
	}

	return &response, nil
}
